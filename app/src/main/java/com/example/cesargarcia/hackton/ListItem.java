package com.example.cesargarcia.hackton;

/**
 * Created by cesargarcia on 10/12/15.
 */
public class ListItem {
    String title;
    String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
