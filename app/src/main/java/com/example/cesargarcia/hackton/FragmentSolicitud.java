package com.example.cesargarcia.hackton;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;


public class FragmentSolicitud extends Fragment {
    ArrayList<ListItem> data = new ArrayList<>();
    private int mYear;
    private int mMonth;
    private int mDay;
    private TextView mDateDisplay;
    static final int DATE_DIALOG_ID = 0;

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            updateDisplay();
        }
    };

    public static FragmentSolicitud newInstance() {
        FragmentSolicitud fragment = new FragmentSolicitud();
        return fragment;
    }

    public FragmentSolicitud() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_solicitud, container, false);

        //CONSTANCIA
        ImageButton buttonConstancia = (ImageButton) v.findViewById(R.id.btConstancia);
        buttonConstancia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater factory = LayoutInflater.from(getContext());
                final View view =
                        factory.inflate(R.layout.dialog_constancia, null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final AlertDialog alert = builder.create();

                alert.setView(view);
                alert.show();

                ImageButton btConfirmar = (ImageButton) view.findViewById(R.id.btConfirmar);
                btConfirmar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        TextView txDirigido = (TextView) view.findViewById(R.id.txDirigido);
                        if (txDirigido.getText().toString().equals("")) {
                            Toast.makeText(getContext(), "Debe indicar a quien va dirigido", Toast.LENGTH_LONG).show();

                        } else {
                            String nombreDirigido = txDirigido.getText().toString();

                            Toast.makeText(getContext(), "Su solicitud se ha procesado exitosamente", Toast.LENGTH_LONG).show();
                            alert.dismiss();
                        }
                    }
                });

                ImageButton btCerrar = (ImageButton) view.findViewById(R.id.imageButtonClose);
                btCerrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            alert.dismiss();
                    }
                });
            }
        });
        //FIN CONSTANCIA

        //VACACIONES

        ImageButton buttonVacaciones = (ImageButton) v.findViewById(R.id.buttonVacaciones);
        buttonVacaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater factory = LayoutInflater.from(getContext());
                final View view =
                        factory.inflate(R.layout.dialog_vacaciones, null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final AlertDialog alert = builder.create();

                alert.setView(view);
                alert.show();

                ImageButton btConfirmar = (ImageButton) view.findViewById(R.id.btConfirmar);
                btConfirmar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        TextView txDirigido = (TextView) view.findViewById(R.id.txDirigido);
                        if (txDirigido.getText().toString().equals("")) {
                            Toast.makeText(getContext(), "Debe indicar la fecha de inicio de vacaciones", Toast.LENGTH_LONG).show();

                        } else {
                            String nombreDirigido = txDirigido.getText().toString();

                            Toast.makeText(getContext(), "Su solicitud se ha procesado exitosamente", Toast.LENGTH_LONG).show();
                            alert.dismiss();
                        }
                    }
                });

                ImageButton btCerrar = (ImageButton) view.findViewById(R.id.imageButtonClose);
                btCerrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });

                ImageButton datePicker = (ImageButton) view.findViewById(R.id.imageButtonPicker);
                datePicker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //showDialog(DATE_DIALOG_ID);
                        Toast.makeText(getContext(), "Aqui muestra el datePicker", Toast.LENGTH_LONG).show();
                    }

                });

                mDateDisplay = (TextView) view.findViewById(R.id.txFecha);

                // Get the current date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                // Display the current date
                updateDisplay();
            }
        });
        //FIN VACACIONES


        return v;

    }

    // Update the date in the TextView
    private void updateDisplay() {
        mDateDisplay.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(mMonth + 1).append("-").append(mDay).append("-")
                .append(mYear).append(" "));
    }

    // Create and return DatePickerDialog
//    @Override
//    protected Dialog onCreateDialog(int id) {
//        switch (id) {
//            case DATE_DIALOG_ID:
//                return new DatePickerDialog(getContext(), mDateSetListener, mYear, mMonth, mDay);
//        }
//        return null;
//    }




}
