package com.example.cesargarcia.hackton;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ListAdapter extends BaseAdapter {

    private ArrayList<ListItem> searchArrayList;

    private LayoutInflater mInflater;

    public ListAdapter(Context context, ArrayList<ListItem> results) {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);

    }

    public int getCount() {
        return searchArrayList.size();
    }

    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.adapter_item, null);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.text = (TextView) convertView.findViewById(R.id.text);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(searchArrayList.get(position).getTitle());
        holder.text.setText(searchArrayList.get(position).getContent());

        return convertView;
    }


     class ViewHolder {
        TextView title;
        TextView text;

    }

}