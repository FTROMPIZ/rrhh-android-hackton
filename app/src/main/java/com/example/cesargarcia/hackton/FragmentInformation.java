package com.example.cesargarcia.hackton;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class FragmentInformation extends Fragment {

    ArrayList<ListItem> data = new ArrayList<>();

    public static FragmentInformation newInstance() {
        FragmentInformation fragment = new FragmentInformation();
        return fragment;
    }

    public FragmentInformation() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_information, container, false);

        Button btEvt = (Button) v.findViewById(R.id.btEventos);
        ImageButton btSeg = (ImageButton) v.findViewById(R.id.btSeguro);
        Button btDir = (Button) v.findViewById(R.id.btDirectorio);
        ImageButton btBrok = (ImageButton) v.findViewById(R.id.btBroker);

        btSeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"Descargar archivo",Toast.LENGTH_LONG).show();
            }
        });

        btDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent DirIntent = new Intent(getContext(), Directorio.class);
                startActivity(DirIntent);
            }
        });


        btBrok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater factory2 = LayoutInflater.from(getContext());
                final View view2 =
                        factory2.inflate(R.layout.dialog_broker, null);
                AlertDialog.Builder builder3 = new AlertDialog.Builder(getContext());
                //   builder2.setTitle(getString(""));
                final AlertDialog alert = builder3.create();

                alert.setView(view2);
                alert.show();
                ImageButton btCerrar = (ImageButton) view2.findViewById(R.id.imageButtonClose);
                btCerrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });
            }
        });


        btEvt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Activity Enventos", Toast.LENGTH_SHORT).show();
            }
        });
//        ListItem list = new ListItem();
//        list.setContent("Esta es una info de prueba , probando . Esta es una info de prueba , probando.Esta es una info de prueba , probando\"");
//        list.setTitle("Titulo");
//        data.add(list);
//
//        ListItem list2 = new ListItem();
//        list2.setContent("Esta es una info de prueba , probando . Esta es una info de prueba , probando.Esta es una info de prueba , probando\"");
//        list2.setTitle("Titulo2");
//        data.add(list2);
//
//        ListView listview = (ListView) v.findViewById(R.id.List);
//        listview.setAdapter(new ListAdapter(getActivity(), data));

        return v;
    }


}
